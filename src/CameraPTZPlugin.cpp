#include "tf/transform_datatypes.h"
#include "gazebo/common/Plugin.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/transport/transport.hh"
#include "gazebo/util/system.hh"
#include "ros/ros.h"
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"
#include "geometry_msgs/Twist.h"
#include "nav_msgs/Odometry.h"
#include <memory>
#include <thread>
#include "fredarobot791547/ptz.h"
#include <tf/transform_broadcaster.h> 
namespace gazebo
{
	class GAZEBO_VISIBLE CameraPTZPlugin : public ModelPlugin
	{
	public:
		CameraPTZPlugin();
		void Load(physics::ModelPtr model, sdf::ElementPtr sdf);
	private:
		int RegisterHinge(int index, const std::string& name);
		void OnUpdate(const common::UpdateInfo& info);
		void OnPTZMessageReceived(const fredarobot791547::ptz::ConstPtr& msg);
		void QueueThread();
	private:
		double _targetPan = 0;
		double _targetTilt = 0;
		ros::CallbackQueue _rosQueue;
		std::thread _rosQueueThread;
		std::unique_ptr<ros::NodeHandle> _rosNode;	
		ros::Subscriber _ptzSub;
		event::ConnectionPtr _updateConnection;
		physics::JointPtr _panJoint;
		physics::JointPtr _tiltJoint;
		physics::ModelPtr _model;
		tf::TransformBroadcaster _tfBroadcaster;
	};
}

using namespace gazebo;

GZ_REGISTER_MODEL_PLUGIN(CameraPTZPlugin)

CameraPTZPlugin::CameraPTZPlugin()
{
	ROS_INFO("Camera PTZ plugin created.");
}

void CameraPTZPlugin::Load(physics::ModelPtr model, sdf::ElementPtr sdf)
{
	_model = model;
	
	// Check for ROS initialisation.
	if (!ros::isInitialized())
	{
		ROS_FATAL_STREAM("A ROS node for Gazebo has not been initialized, unable to load plugin. "
			<< "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in the gazebo_ros package)");
		return;
	}
	
	ROS_INFO("Camera PTZ plugin initialised.");
	
	// Set up ROS node
	_rosNode.reset(new ros::NodeHandle);
	
	auto sub_opts = ros::SubscribeOptions::create<fredarobot791547::ptz> (
		"/robot/ptz",
		10,
		boost::bind(&CameraPTZPlugin::OnPTZMessageReceived, this, _1),
		ros::VoidPtr(),
		&_rosQueue
	);
	
	_panJoint = _model->GetJoint("camera_pan_joint");
	_tiltJoint = _model->GetJoint("camera_tilt_joint");
	
	if (!_panJoint || !_tiltJoint) ROS_WARN("camera pan and tilt joints not found");
	
	_ptzSub = _rosNode->subscribe(sub_opts);
	_updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&CameraPTZPlugin::OnUpdate, this, _1));
	_rosQueueThread = std::thread(std::bind(&CameraPTZPlugin::QueueThread, this));

}

void CameraPTZPlugin::QueueThread()
{
	static const double timeout = 0.1;
	while (_rosNode->ok()) {
		_rosQueue.callAvailable(ros::WallDuration(timeout));
	}
}

void CameraPTZPlugin::OnUpdate(const common::UpdateInfo& info)
{
	if (!_panJoint || !_tiltJoint) return;
	
	auto pan = _panJoint->GetAngle(0);
	auto tilt = _tiltJoint->GetAngle(0);
	
	auto panErr = math::Angle(_targetPan) - pan;
	auto tiltErr = math::Angle(_targetTilt) - tilt;
	
	_panJoint->SetForce(0, panErr.Radian() * 20);
	_tiltJoint->SetForce(0, tiltErr.Radian() * 20);
	
	
	tf::Transform transform1;
	transform1.setOrigin(tf::Vector3(0,0,0));
	transform1.setRotation(tf::createQuaternionFromYaw(pan.Radian()));
	
	_tfBroadcaster.sendTransform(tf::StampedTransform(transform1, ros::Time::now(), "base_link", "camera_pan_joint"));
	
	tf::Transform transform2;
	transform2.setOrigin(tf::Vector3(0,0,0.8-0.13));
	transform2.setRotation(tf::createQuaternionFromRPY(0, -tilt.Radian(), 0));
	
	_tfBroadcaster.sendTransform(tf::StampedTransform(transform2, ros::Time::now(), "camera_pan_joint", "camera_tilt_joint"));
}

void CameraPTZPlugin::OnPTZMessageReceived(const fredarobot791547::ptz::ConstPtr& msg)
{
	_targetPan = msg->pan;
	_targetTilt = math::clamp(msg->tilt, -1.5708, 1.5708);
}
