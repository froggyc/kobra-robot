#include "tf/transform_datatypes.h"
#include "gazebo/common/Plugin.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/transport/transport.hh"
#include "gazebo/util/system.hh"
#include "ros/ros.h"
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"
#include "geometry_msgs/Twist.h"
#include "nav_msgs/Odometry.h"
#include <memory>
#include <thread>
#include <tf/transform_broadcaster.h> 

namespace gazebo
{
	class GAZEBO_VISIBLE SkidSteerPlugin : public ModelPlugin
	{
	public:
		enum { RIGHT_FRONT, RIGHT_BACK, LEFT_FRONT, LEFT_BACK, N_WHEELS };
		SkidSteerPlugin();
		void Load(physics::ModelPtr model, sdf::ElementPtr sdf);
	private:
		int RegisterHinge(int index, const std::string& name);
		void OnUpdate(const common::UpdateInfo& info);
		void OnCommandMessageReceived(const geometry_msgs::TwistConstPtr& msg);
		void QueueThread();
	private:
		double _lastTime = -1;
		ros::CallbackQueue _rosQueue;
		std::thread _rosQueueThread;
		std::unique_ptr<ros::NodeHandle> _rosNode;	
		ros::Subscriber _commandSub;
		ros::Publisher _odometryPub;
		event::ConnectionPtr _updateConnection;
		physics::JointPtr _hinges [N_WHEELS];
		physics::ModelPtr _model;
		double _wheelSeparation;
		double _wheelRadius;
		tf::TransformBroadcaster _tfBroadcaster;
	};
}

using namespace gazebo;

GZ_REGISTER_MODEL_PLUGIN(SkidSteerPlugin)

SkidSteerPlugin::SkidSteerPlugin()
{
	ROS_INFO("Skidsteer plugin created.");
}

void SkidSteerPlugin::Load(physics::ModelPtr model, sdf::ElementPtr sdf)
{
	_model = model;
	
	// Check for ROS initialisation.
	if (!ros::isInitialized())
	{
		ROS_FATAL_STREAM("A ROS node for Gazebo has not been initialized, unable to load plugin. "
			<< "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in the gazebo_ros package)");
		return;
	}
	
	ROS_INFO("Skidsteer plugin initialised.");
	
	// Set up ROS node
	_rosNode.reset(new ros::NodeHandle);
	
	int err = 0;
	err += RegisterHinge(RIGHT_FRONT, "right_front_wheel_hinge");
	err += RegisterHinge(RIGHT_BACK,  "right_back_wheel_hinge");
	err += RegisterHinge(LEFT_FRONT,  "left_front_wheel_hinge");
	err += RegisterHinge(LEFT_BACK,   "left_back_wheel_hinge");
	
	if (err > 0)
	{
		return;
	}
	
	_wheelSeparation = _hinges[RIGHT_FRONT]->GetAnchor(0).Distance(_hinges[LEFT_FRONT]->GetAnchor(0));
	
	physics::EntityPtr wheelLink = boost::dynamic_pointer_cast<physics::Entity>(_hinges[RIGHT_FRONT]->GetChild() );
	if (wheelLink)
	{
		math::Box bb = wheelLink->GetBoundingBox();
		_wheelRadius = bb.GetSize().GetMax() * 0.5;
	}
	
	// subscribe to velocity command topic
	auto sub_opts = ros::SubscribeOptions::create<geometry_msgs::Twist> (
		"/robot/cmd_velocity",
		10,
		boost::bind(&SkidSteerPlugin::OnCommandMessageReceived, this, _1),
		ros::VoidPtr(),
		&_rosQueue
	);
	_commandSub = _rosNode->subscribe(sub_opts);
	
	_odometryPub = _rosNode->advertise<nav_msgs::Odometry>("robot/odometry", 10);
	
	_updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&SkidSteerPlugin::OnUpdate, this, _1));
	
	_rosQueueThread = std::thread(std::bind(&SkidSteerPlugin::QueueThread, this));

}

void SkidSteerPlugin::QueueThread()
{
	static const double timeout = 0.1;
	while (_rosNode->ok()) {
		_rosQueue.callAvailable(ros::WallDuration(timeout));
	}
}

void SkidSteerPlugin::OnUpdate(const common::UpdateInfo& info)
{
	if (_lastTime < 0) _lastTime = info.simTime.Double();
	
	double angv_r = 0.5 * (_hinges[RIGHT_BACK]->GetVelocity(0) + _hinges[RIGHT_FRONT]->GetVelocity(0));
	double angv_l = 0.5 * (_hinges[LEFT_BACK]->GetVelocity(0) + _hinges[LEFT_FRONT]->GetVelocity(0));
	
	double vr = angv_r * _wheelRadius;
	double vl = angv_l * _wheelRadius;
	
	double vel = (vr + vl) * 0.5;
	double angv = (vl - vr) / _wheelSeparation;
	
	double time = info.simTime.Double();
	double dt = time - _lastTime;
	_lastTime = time;
	
	nav_msgs::Odometry odom;
	
	odom.header.stamp = ros::Time(time);
	//odom.header.frame_id = "odom";
	
	//set the position
	odom.pose.pose.position.x = vel * dt;
	odom.pose.pose.position.y = 0.0;
	odom.pose.pose.position.z = 0.0;
	odom.pose.pose.orientation = tf::createQuaternionMsgFromYaw (angv*dt);
	
	//set the velocity
	//odom.child_frame_id = "base_link";
	odom.twist.twist.linear.x = vel;
	odom.twist.twist.angular.z = angv;
	
	_odometryPub.publish(odom);
	
	auto pose = _model->GetLink("chassis")->GetWorldPose();
	tf::Transform transform;
	
	transform.setOrigin(tf::Vector3(pose.pos.x, pose.pos.y, pose.pos.z));
	transform.setRotation(tf::Quaternion(pose.rot.x, pose.rot.y, pose.rot.z, pose.rot.w));
	
	_tfBroadcaster.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", "robot_gt"));
}

void SkidSteerPlugin::OnCommandMessageReceived(const geometry_msgs::TwistConstPtr& msg)
{
	for (int i; i < N_WHEELS; ++i)
	{
		_hinges[i]->SetParam("fmax", 0, 5.0);
	}
	
	
	
	double linear = math::clamp(msg->linear.x, -1.0, 1.0);
	double angular = math::clamp(msg->angular.z, -1.0, 1.0);
	
	double radius = linear / angular;
	
	double vr = linear + angular * _wheelSeparation * 0.5;
	double vl = linear - angular * _wheelSeparation * 0.5;
	
	_hinges[RIGHT_BACK	]->SetVelocity(0, vr / _wheelRadius);
	_hinges[RIGHT_FRONT	]->SetVelocity(0, vr / _wheelRadius);
	_hinges[LEFT_BACK	]->SetVelocity(0, vl / _wheelRadius);
	_hinges[LEFT_FRONT	]->SetVelocity(0, vl / _wheelRadius);
}

int SkidSteerPlugin::RegisterHinge(int index, const std::string& name)
{
	if (index < 0 or index >= N_WHEELS)
	{
		gzerr << "Joint index " << index <<  " out of bounds [0, "
			<< N_WHEELS << "] in model " << _model->GetName()
			<< "." << std::endl;
		return 1;
	}

	_hinges[index] = _model->GetJoint(name);
	if (!_hinges[index])
	{
		gzerr << "Unable to find the " << name
			<<  " joint in model " << _model->GetName() << "." << std::endl;
		return 1;
	}

	return 0;
}
