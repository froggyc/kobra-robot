Instructions:

- put the project as a folder inside your catkin workspace `catkin_ws/src/fredarobot791547`

- run catkin_make inside your `catkin_ws/`

- source the setup.sh file if you haven't already `source devel/setup.sh`

- `roslaunch fredarobot791547 kobra.launch`

In another terminal: 

- if you don't have it yet install `sudo apt install ros-kinetic-teleop-twist-keyboard`

- run `rosrun teleop_twist_keyboard teleop_twist_keyboard.py cmd_vel:=/robot/cmd_velocity

- follow the instructions to control the robot using the keyboard

In another terminal:

- source again your catkin `setup.sh` like before.

- run `rostopic pub /robot/pvz fredarobot791547/pvz -r 1 "{pan: 1, tilt: 1, zoom: 0}"` (or insert your own values)

- observe the camera rotating to the specified pan and tilt in radians.

In another terminal:

- run `rosrun rviz rviz`

- set world as fixed frame.

- add a display of /robot/pose as pose.

- add a display of tf hierarchy

- while you move the robot around observe the robot_gt frame move in concordance with the robot in the simulation, and the /robot/pose visualisator move in accordance to the odometry.

- (unfortunately since point 4 (navigation) is unfinished the base link has not been linked to the world frame yet.)

- set base_link as fixed frame to see all the tf childed to it

- add a display of topic /robot/laser_scan as scan results

- add a display of topic /robot/image as image (not camera!)

- drive the robot around again and observe the laser scan, the transformation frames, and the camera image.
